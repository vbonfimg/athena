/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "InDetSimEvent/SiHit.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "SCT_ReadoutGeometry/SCT_DetectorManager.h"
#include "PixelReadoutGeometry/PixelDetectorManager.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "GeoPrimitives/CLHEPtoEigenConverter.h"


inline GeoSiHit::GeoSiHit (const SiHit & h) {
  m_hit = &h;
}

inline HepGeom::Point3D<double> GeoSiHit::getGlobalPosition() const {

  int Barrel = m_hit->getBarrelEndcap();
  if (Barrel== 1) Barrel = -2;
  Identifier id;
  const InDetDD::SiDetectorElement *geoelement=NULL;
  if (m_hit->isPixel()) {
    
    id = pixID()->wafer_id(Barrel,
			m_hit->getLayerDisk(), 
			m_hit->getPhiModule(), 
			m_hit->getEtaModule());
    geoelement = pixMgr()->getDetectorElement(id);      
  }
  else {
    id = sctID()->wafer_id(Barrel,
			m_hit->getLayerDisk(), 
			m_hit->getPhiModule(), 
			m_hit->getEtaModule(), 
			m_hit->getSide() );
    geoelement = sctMgr()->getDetectorElement(id);      
  }
  
  
  if (geoelement) {

    const HepGeom::Point3D<double> globalStartPos = Amg::EigenTransformToCLHEP(geoelement->transformHit()) * HepGeom::Point3D<double>(m_hit->localStartPosition());
    
    double x=globalStartPos.x();
    double y=globalStartPos.y();
    double z=globalStartPos.z();
    return HepGeom::Point3D<double>(x,y,z);
  }

  return HepGeom::Point3D<double>(0.0,0.0,0.0);
}

